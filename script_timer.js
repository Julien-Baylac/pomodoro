//Timer
let min = 24;
let sec = 60;
let timeToWork = true;
let interval;
let timerActive = false;
let numeroEtape = 1

function timer() {
	sec --;

	$('#minutes').html(min);
	if (sec < 10) {$('#seconds').html('0' + sec)}
	else {$('#seconds').html(sec)}

	if (sec == 0) {
		if (min == 0) {
			musicInGame();
			if (timeToWork) {
				showModal('Etape');
				timeToWork = false;
				if (numeroEtape % 4 == 0) {min = 10}
				else {min = 5}
				numeroEtape ++;
			}
			else {
				showModal('Pause');
				timeToWork = true;
				min = 25;
			}
		}
		min --
		sec = 60
	}
}


//Buttons
$('#start').click(function() {
	if(!timerActive) {
		interval = setInterval(timer,1000);
		timerActive = true;
	}
})

$('#stop').click(function() {
	clearInterval(interval);
	timerActive = false;
})

$('#reset').click(function() {
	clearInterval(interval);
	timerActive = false;
	min = 24;
	sec = 60;
	timeToWork = true;
	$('#minutes').html('25');
	$('#seconds').html('00');
	numeroEtape = 1;
})


//Modal
function showModal (situation) {
	$("#nb" + situation).html(numeroEtape);
	UIkit.modal("#modal" + situation).show();

}


//Space bar
let imWritingATask = false;
$('#entertask').focus(function() {imWritingATask = true})
$('#entertask').blur(function() {imWritingATask = false})


document.body.onkeyup = function(e){
	if (!imWritingATask) {
	    if(e.keyCode == 32){
	        if (timerActive) {
	        	timerActive = false;
	        	clearInterval(interval)
	        }
	        else {
	        	timerActive = true;
	        	interval = setInterval(timer,1000)
	        }
	    }
	}
}


// Add tasks
let newtask;
let allTasks = [];
let nbTasksDone = 0;

function addATask() {
	newtask = $('#entertask')[0].value;
	if (newtask != "") {
		$('#entertask')[0].value = "";
		allTasks.push(false);

		var labeltask = document.createElement("LABEL");

		var inputtask = document.createElement("INPUT");
		inputtask.className = "uk-checkbox";
		inputtask.type = "checkbox";
		inputtask.id = 'check' + allTasks.length;

		var texttask = document.createTextNode(' ' + newtask);

		var br = document.createElement("BR");

		labeltask.appendChild(inputtask);
		labeltask.appendChild(texttask);
		$('#listOfTasks').append(labeltask);
		$('#listOfTasks').append(br);


		$('#' + inputtask.id).click(function() {
			var nbtask = parseInt(inputtask.id.replace('check', '')) - 1;

			if (allTasks[nbtask]) {
				nbTasksDone --;
				allTasks[nbtask] = false;
			}
			else {
				nbTasksDone ++;
				allTasks[nbtask] = true;
				if (nbTasksDone == allTasks.length) {
					clearInterval(interval);
					showModal('End');
				}
			}
		})
	}
}


// read the music
function musicInGame() {
    var audio = new Audio('signal.mp3');
    audio.play();
}
